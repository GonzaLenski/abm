import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class BaseDeDatos {

	public static void agregarClienteALaTabla(String nombre, String apellido, String telefono, String mail,
			String direccion, String localidad) throws SQLException, ClassNotFoundException {
		try {
			// Creo la conexi�n a la base de datos dando como par�metro el String de
			// conexi�n y el Driver
			String myDriver = "org.gjt.mm.mysql.Driver";
			String myUrl = "jdbc:mysql://localhost/abm";
			Class.forName(myDriver);
			// Doy los par�metros necesarios para la conexi�n con la BD, usuario y
			// contrase�a
			Connection conn = (Connection) DriverManager.getConnection(myUrl, "root", "");
			String query = "insert into cliente (nombre, apellido, telefono, mail, direccion, localidad)"
					+ " values (?, ?, ?, ?, ?, ?)";

			// insert mediante preparedStatement donde doy como par�metro el query y los
			// valores del objeto cliente
			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, nombre);
			preparedStmt.setString(2, apellido);
			preparedStmt.setString(3, telefono);
			preparedStmt.setString(4, mail);
			preparedStmt.setString(5, direccion);
			preparedStmt.setString(6, localidad);

			// Ejecuto el preparedStatement, de esta manera, inserto los valores a la base
			// de datos
			preparedStmt.executeUpdate();
			// cierro la conexi�n.
			conn.close();
		} catch (SQLException e) {
			// log the exception
			System.out.println("Se ha generado la siguiente excepci�n:");
			System.out.println(e.getMessage());
		}
	}

	// Realizamos update, sincronizamos valores de la tabla, con nuevos valores
	// dados como par�metro.
	public static void updateClientePorID(int id, String nombre, String apellido, String telefono, String mail,
			String direccion, String localidad) throws SQLException, ClassNotFoundException {
		try {
			// Creo la conexi�n a la base de datos dando como par�metro el String de
			// conexi�n y el Driver
			String myDriver = "org.gjt.mm.mysql.Driver";
			String myUrl = "jdbc:mysql://localhost/abm";
			Class.forName(myDriver);
			// Doy los par�metros necesarios para la conexi�n con la BD, usuario y
			// contrase�a
			Connection conn = (Connection) DriverManager.getConnection(myUrl, "root", "");
			// Query de inserci�n de datos en la tabla cliente
			String query = "UPDATE cliente SET nombre = ?, apellido = ?, telefono=? , mail = ?, direccion = ?, localidad = ?  WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setString(1, nombre);
			ps.setString(2, apellido);
			ps.setString(3, telefono);
			ps.setString(4, mail);
			ps.setString(5, direccion);
			ps.setString(6, localidad);
			ps.setInt(7, id);

			ps.executeUpdate();
			ps.close();
		} catch (SQLException | ClassNotFoundException e) {
			System.out.println("Se ha generado la siguiente excepci�n:");
			System.out.println(e.getMessage());

		}
	}

	// borrar tabla contenido tabla Clientes
	public static void borrarTodosLosRegistrosDeLaTablaClientes() throws SQLException, ClassNotFoundException {
		try {
			// Creo la conexi�n a la base de datos dando como par�metro el String de
			// conexi�n y el Driver
			String myDriver = "org.gjt.mm.mysql.Driver";
			String myUrl = "jdbc:mysql://localhost/abm";
			Class.forName(myDriver);
			// Doy los par�metros necesarios para la conexi�n con la BD, usuario y
			// contrase�a
			Connection conn = DriverManager.getConnection(myUrl, "root", "");

			String sql = "DELETE FROM cliente";
			PreparedStatement prest = conn.prepareStatement(sql);
			prest.executeUpdate();
			conn.close();

		} catch (SQLException s) {
			System.out.println("No se pudo ejecutar el SQL!");
		}
	}

	public static void agregarProductoALaTabla(String nombre, int cantidad, double precio)
			throws SQLException, ClassNotFoundException {
		try {
			// Creo la conexi�n a la base de datos dando como par�metro el String de
			// conexi�n y el Driver
			String myDriver = "org.gjt.mm.mysql.Driver";
			String myUrl = "jdbc:mysql://localhost/abm";
			Class.forName(myDriver);
			// Doy los par�metros necesarios para la conexi�n con la BD, usuario y
			// contrase�a
			Connection conn = (Connection) DriverManager.getConnection(myUrl, "root", "");
			String query = " insert into producto (nombre, cantidad, precio)" + " values (?, ?, ?)";

			// insert mediante preparedStatement donde doy como par�metro el query y los
			// valores del objeto producto
			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, nombre);
			preparedStmt.setInt(2, cantidad);
			preparedStmt.setDouble(3, precio);

			// Ejecuto el preparedStatement, de esta manera, inserto los valores a la base
			// de datos
			preparedStmt.executeUpdate();
			// cierro la conexi�n.
			conn.close();
		} catch (SQLException se) {
			// log the exception
			throw se;
		}
	}

	// Realizamos update, sincronizamos valores de la tabla producto", con nuevos
	// valores dados como par�metro. Prestar atenci�n que le damos como par�metro el
	// "id" del producto

	public static void updateProductoPorID(int id, String Nombre, int cantidad, double precio)
			throws SQLException, ClassNotFoundException {
		try {
			// Creo la conexi�n a la base de datos dando como par�metro el String de
			// conexi�n y el Driver
			String myDriver = "org.gjt.mm.mysql.Driver";
			String myUrl = "jdbc:mysql://localhost/abm";
			Class.forName(myDriver);
			// Doy los par�metros necesarios para la conexi�n con la BD, usuario y
			// contrase�a
			Connection conn = (Connection) DriverManager.getConnection(myUrl, "root", "");
			// Query de inserci�n de datos en la tabla cliente
			String query = "UPDATE producto SET nombre = ?, cantidad = ?, precio = ?  WHERE id = ?";

			// create our java preparedstatement using a sql update query
			PreparedStatement ps = conn.prepareStatement(query);

			// set the preparedstatement parameters

			ps.setString(1, Nombre);
			ps.setInt(2, cantidad);
			ps.setDouble(3, precio);
			ps.setInt(4, id);

			// call executeUpdate to execute our sql update statement
			ps.executeUpdate();
			ps.close();
		} catch (SQLException | ClassNotFoundException e) {
			// log the exception
			System.err.println("Se ha generado la siguiente excepci�n:");
			System.err.println(e.getMessage());

		}
	}

	public static void borrarTodosLosRegistrosDeLaTablaProductos() throws SQLException, ClassNotFoundException {
		try {
			// Creo la conexi�n a la base de datos dando como par�metro el String de
			// conexi�n y el Driver
			String myDriver = "org.gjt.mm.mysql.Driver";
			String myUrl = "jdbc:mysql://localhost/abm";
			Class.forName(myDriver);
			// Doy los par�metros necesarios para la conexi�n con la BD, usuario y
			// contrase�a
			Connection conn = DriverManager.getConnection(myUrl, "root", "");

			String sql = "DELETE FROM PRODUCTO";
			PreparedStatement prest = conn.prepareStatement(sql);
			prest.executeUpdate();
			conn.close();

		} catch (SQLException s) {
			System.out.println("SQL statement is not executed!");
		}
	}

}
